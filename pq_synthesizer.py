# -*- coding: utf-8 -*-
"""
Created on Tue Jul  8 11:21:51 2014

The script can be called from command line in 2 different modes:

-training:
    Usage:
    python pq_synthesizer.py train 'path to pq-file' 'path to output model file'
    
    'path to pq-file' is the path the single appliance measurement of the
    appliance to synthesize. Should be csv where rows denote time points and
    columns contain P and Q respectively. No header. 50Hz is recommended.
    
    'path to model file' is the path where the output model file should be stored
    
-synthesizing:
    Usage:
    python pq_synthesizer.py synthesize 'path to model file' length 'path to output pq-file'
    
    'path to model file' is the path to the model (that was created with 'train')
    
    length is the number of data points you wish to synthesize
    
    'path to output pq-file' denotes to where the synthetic appliance run should
    written. The output is a csv file in the same format as the input to 'train'


@author: henning
"""

from sklearn.mixture import GMM
from helpers import conditionalProb
import numpy as np
import sys
import pickle



class pq_synthesizer:
    
    def __init__(self, quantization = 100, history_len = 10):
        self.trained = False
        self.quantizer = GMM(n_components=quantization, covariance_type='full', thresh=0.01, min_covar=0.001, n_iter=100, n_init=5)
        self.history_len = history_len
        self.state_trans = conditionalProb(history_len)
        
    
    
    def train(self,pq_data):
        #The PQ synthesizer expects 50Hz P and Q data in matrix format
        #The size of the matrix should be (len, 2)
        
        print 'Quantizing the PQ data'
        self.quantizer.fit(pq_data)
        state_sequence = self.quantizer.predict(pq_data)
        
        #remembering the first states
        self.start_history = state_sequence[0:self.history_len]
        
        
        print 'Quantization done - fitting the state transition model'
        for i in xrange(len(state_sequence)-self.history_len):
            self.state_trans.add(state_sequence[i:i+self.history_len],state_sequence[i+self.history_len])
            
        print 'State transition model fitted - learning done'
        
        self.trained = True
        
        return state_sequence
        
        
        
    def createSignal(self, length):
        if self.trained == False:
            raise StandardError('Synthesizer has not yet been trained')
            
        syn_state_seq = np.zeros(length)
        syn_state_seq[0:self.history_len] = self.start_history
        
        print 'Creating synthetic state sequence'
        for i in xrange(length-self.history_len):
            syn_state_seq[i+self.history_len] = self.state_trans.random_pick(syn_state_seq[i:i+self.history_len])
            
        print 'Turning state sequence into observation sequence'            
        syn_seq = np.zeros((length,2))
        for i in xrange(length):
            state = int(syn_state_seq[i])
            mean = self.quantizer.means_[state]
            cov = self.quantizer.covars_[state]
            #print np.random.multivariate_normal(mean,cov)
            syn_seq[int(i),:] = np.random.multivariate_normal(mean,cov)
            
        return syn_seq, syn_state_seq
            
            
            
if __name__ == '__main__':
    if sys.argv[1] == 'train':
        pq = np.loadtxt(sys.argv[2], delimiter=',')
        synti = pq_synthesizer()
        synti.train(pq)
        pickle.dump(synti,open(sys.argv[3],'wb'))
    elif sys.argv[1] == 'synthesize':
        print 'Loading model'
        model = pickle.load(open(sys.argv[2],'rb'))
        syn, state = model.createSignal(int(sys.argv[3]))
        np.savetxt(sys.argv[4], syn, delimiter=",")