# -*- coding: utf-8 -*-
"""
Created on Mon Jun  2 10:15:46 2014

@author: henning
"""

import random
import numpy
from matplotlib import *


class conditionalProb:
    
    def __init__(self, condition_length):
        self.conditions = dict()
        self.condition_length = condition_length
    
    def add(self, condition, value):
        if not len(condition) == self.condition_length:
            raise StandardError('Input dimension mismatch')
        self.add_recursive(condition, value, self.conditions)
        
    def add_recursive(self, condition, value, conditions):
        if len(condition) == 0:
            if conditions.has_key(value):
                conditions[value]+=1
                #print 'Can reuse', conditions[value]
            else:
                conditions[value]=1
        else:
            #print condition[0]
            if not conditions.has_key(condition[0]):
                conditions[condition[0]] = dict()
            self.add_recursive(condition[1:], value, conditions[condition[0]])
            
    def get_recursive(self, condition, conditions):
        #recursively retrieves the dict which stores the info about the condition
        #returns None if the condition is not known
        if len(condition) == 0:
            return conditions
        else:
            if conditions.has_key(condition[0]):
                return self.get_recursive(condition[1:], conditions[condition[0]])
            return None    
            
    def prob(self, condition, value):
        p_dist = self.get_recursive(condition, self.conditions)
        if p_dist == None or not p_dist.has_key(value):
            return 0
        else:
            return (p_dist[value]+0.0)/sum(p_dist.values())
            
    def random_pick(self, condition):
        p_dist = self.get_recursive(condition, self.conditions)
        if p_dist == None:
            print 'Condition:', condition
            raise StandardError('Unknown condition - cannot pick random')
            
        #print 'Chosing from',len(p_dist.values())
            
        r = random.uniform(0, sum(p_dist.values()))
        s = 0.0
        for k, w in p_dist.iteritems():
            s += w
            if r < s: return k
        return k
        
        
        
        
#quantization: symbolic, find binsizes that are equi-probable    
def quantize(vals, desired_bins = 80):
    desired_prob = 1.0/desired_bins
    probs, ranges = numpy.histogram(vals,desired_bins*500)
    ranges[len(ranges)-1]*=1.1
    probs = (probs+0.0)/len(vals)
    
    symbols = list()
    
    i_up = 0
    i_low = 0
    
    for i in xrange(0,desired_bins):
        prob = 0
        while(prob<desired_prob and i_up<len(probs)):
            prob+=probs[i_up]
            i_up+=1
        
        symbols.append((ranges[i_low],ranges[i_up]))
        i_low = i_up

    out = list()
    
    for v in vals:
        for r in symbols:
            if (v >= r[0] and v < r[1]) or (v == r[0] and v == r[1]):
                out.append(round(numpy.mean(r),2))
                break
                
    return out
    
    
            


#autocorrelation for determining h:

def plot_acf(series):
    n = len(series)
    #n = 100
    data = numpy.asarray(series)
    mean = numpy.mean(data)
    c0 = numpy.sum((data - mean) ** 2) / float(n)

    def r(h):
        acf_lag = ((data[:n - h] - mean) * (data[h:] - mean)).sum() / float(n) / c0
        return round(acf_lag, 3)
    x = numpy.arange(n) # Avoiding lag 0 calculation
    acf_coeffs = map(r, x)
    plot(acf_coeffs)
    axhline(2/sqrt(n))
    axhline(-2/sqrt(n))
        